package dao;

import java.io.Serializable;
import javax.annotation.Resource;
import javax.inject.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;


@Singleton
public class DaoOrmImpl  implements Dao, Serializable{
   
    @PersistenceContext
    private EntityManager em; 
 
    @Resource private UserTransaction utx; 
    
    @Override
    public void enregistrerEntite(Object entite) {
        
        try {utx.begin();} catch (Exception ex) {afficherErreur(ex);}
        em.persist(entite);
        try { utx.commit();} catch (Exception ex) {afficherErreur(ex);}
    }
    
    @Override
    public void supprimerEntite(Object entite) {
        
        try {utx.begin();} catch (Exception ex) {afficherErreur(ex);}
        Object asupp=em.merge(entite);
        em.remove(asupp);
        try { utx.commit();} catch (Exception ex) {afficherErreur(ex);}
    }
   
    @Override
 
    public void repercuterMAJ(Object entite) {
        
        try {utx.begin();} catch (Exception ex) {afficherErreur(ex);}
        Object o=em.merge(entite); 
        try { utx.commit();} catch (Exception ex) {afficherErreur(ex);}
    }
    
    @Override
    public void detache(Object entite) {
      
        try {utx.begin();} catch (Exception ex) {afficherErreur(ex);}    
        em.detach(entite); 
        try { utx.commit();} catch (Exception ex) {afficherErreur(ex);}
    }
     
    @Override
    public void rafraichir(Object entite) {
        
        try {utx.begin();} catch (Exception ex) {afficherErreur(ex);}  
        em.refresh(entite);
        try { utx.commit();} catch (Exception ex) {afficherErreur(ex);}
    }

    private void afficherErreur(Exception ex) {
        System.out.println(ex.getMessage());
    }
    
    
    @Override
    public void debuterTransaction(){
    
     try {utx.begin();} catch (Exception ex) {afficherErreur(ex);}  
    }
    
    @Override
    public void validerTransaction(){
     try { utx.commit();} catch (Exception ex) {afficherErreur(ex);}
    }

    @Override
    public EntityManager getEm() {
        return em;
    }
    
    
}


