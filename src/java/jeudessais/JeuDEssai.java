package jeudessais;

/**
 *
 * @author rsmon
 */

public interface JeuDEssai {

    void remplirBDD();
    void viderBDD();

    String reinitialiserBDD();
    
}
