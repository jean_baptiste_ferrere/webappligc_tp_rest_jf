package jeudessais;

import dao.Dao;
import javax.enterprise.inject.Alternative;
import javax.inject.Inject;
import javax.inject.Singleton;

@Alternative
@Singleton
public class JeuDEssaiImpl1 implements JeuDEssai {

    @Inject Dao dao;
    
    @Override
    public void   remplirBDD() {
   
     dao.debuterTransaction();
 
      executeRequeteSQL("insert into Region values('NPDC','Nord Pas De Calais')");
      executeRequeteSQL("insert into Region values('CA','Champagne Ardennes')");
      executeRequeteSQL("insert into Region values('PIC','Picardie')");
      
      executeRequeteSQL("insert into Client values(101,'Arras','Dupont Jean','NPDC')");
      executeRequeteSQL("insert into Client values(102,'Hirson','Legrand Martin','PIC')");
      executeRequeteSQL("insert into Client values(103,'Calais','Martin Sophie','NPDC')");
      executeRequeteSQL("insert into Client values(104,'Lens','Lemortier Jean-Pierre','NPDC')");
      executeRequeteSQL("insert into Client values(105,'Rouvroi','Laurent Fabrice','CA')");
        
      executeRequeteSQL("insert into Commande values(34878,'05/07/2014','R',101)");
      executeRequeteSQL("insert into Commande values(34526,'01/12/2014','R',102)");
      executeRequeteSQL("insert into Commande values(34600,'05/19/2014','R',101)");
      executeRequeteSQL("insert into Commande values(34650,'06/09/2014','R',101)");
      executeRequeteSQL("insert into Commande values(35000,'06/10/2014','R',101)");
      executeRequeteSQL("insert into Commande values(35010,'06/10/2014','L',101)");
      executeRequeteSQL("insert into Commande values(35020,'06/12/2014','L',101)");
      executeRequeteSQL("insert into Commande values(35030,'06/14/2014','R',105)");
      executeRequeteSQL("insert into Commande values(35040,'10/24/2014','R',104)");
        
        
      executeRequeteSQL("insert into CategorieProduit values('GEM','Gros Electro-Ménager')");
      executeRequeteSQL("insert into CategorieProduit values('PEM','Petit Electro-Ménager')");
      executeRequeteSQL("insert into CategorieProduit values('INF','Informatique')");
        
        
      executeRequeteSQL("insert into Produit values('RBM1','Robot Mixer',167,'PEM')");
      executeRequeteSQL("insert into Produit values('LL1','Lave Linge AAA',455,'GEM')");
      executeRequeteSQL("insert into Produit values('LL2','Lave Linge BBB',564,'GEM')");
      executeRequeteSQL("insert into Produit values('TBL1','Tablette FG',167,'INF')");
      executeRequeteSQL("insert into Produit values('LV1','Lave Vaiselle',765,'GEM')");
      executeRequeteSQL("insert into Produit values('EP1','Ecran plat LCD HHHH',145.5,'INF')");
      executeRequeteSQL("insert into Produit values('IMPL1','Imprimante Laser',345.8,'INF')");
      
      
      executeRequeteSQL("insert into LigneDeCommande(id,numcom,refprod,qtecom) values(1,34878,'TBL1',2)");
      executeRequeteSQL("insert into LigneDeCommande(id,numcom,refprod,qtecom) values(2,34526,'LL2',1)");
      executeRequeteSQL("insert into LigneDeCommande(id,numcom,refprod,qtecom) values(3,34526,'RBM1',1)");
      executeRequeteSQL("insert into LigneDeCommande(id,numcom,refprod,qtecom) values(4,34600,'TBL1',1)");
      executeRequeteSQL("insert into LigneDeCommande(id,numcom,refprod,qtecom) values(5,34650,'LL1',1)");
      executeRequeteSQL("insert into LigneDeCommande(id,numcom,refprod,qtecom) values(6,35000,'LV1',1)");
      executeRequeteSQL("insert into LigneDeCommande(id,numcom,refprod,qtecom) values(7,35010,'IMPL1',2)");
      executeRequeteSQL("insert into LigneDeCommande(id,numcom,refprod,qtecom) values(8,35020,'EP1',1)");
      executeRequeteSQL("insert into LigneDeCommande(id,numcom,refprod,qtecom) values(9,35030,'TBL1',1)");
      executeRequeteSQL("insert into LigneDeCommande(id,numcom,refprod,qtecom) values(10,35030,'EP1',1)");
      executeRequeteSQL("insert into LigneDeCommande(id,numcom,refprod,qtecom) values(11,35040,'LL1',1)");
      executeRequeteSQL("insert into LigneDeCommande(id,numcom,refprod,qtecom) values(12,35040,'EP1',1)");   
        
     dao.validerTransaction();   
  }

    @Override
    public void viderBDD() {
      
     dao.debuterTransaction();
        
      executeRequeteSQL("delete from LigneDeCommande");
      executeRequeteSQL("delete from Commande");
      executeRequeteSQL("delete from Produit");
      executeRequeteSQL("delete from Client");
      executeRequeteSQL("delete from Region");
      executeRequeteSQL("delete from CategorieProduit");
       
     dao.validerTransaction();
    }

    @Override
    public String reinitialiserBDD(){
    
      viderBDD();
      remplirBDD();
      
      return "Base de donnees reinitialisee( en SQL natif via EclipseLink)";
    }
    
    
    private void executeRequeteSQL(String requeteSQL){
    
     dao.getEm().createNativeQuery(requeteSQL).executeUpdate(); 
    
    }
}
