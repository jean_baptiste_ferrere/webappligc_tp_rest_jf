package webservice.rest.initbase;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import jeudessais.JeuDEssai;

@Stateless
@Path("tests")
public class ServiceRestInitBase  {
    
    @Inject
    private JeuDEssai jeuDEssai;
    
    @GET
    @Path( "jeudessais/reinitbase")
    public String reinitbase(){
   
        return jeuDEssai.reinitialiserBDD();
    } 

}  
