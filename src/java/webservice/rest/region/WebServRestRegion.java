
package webservice.rest.region;

import dto.region.ResumeRegion;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import webservice.modele.ModeleWebServRegion;

/**
 *
 * @author rsmon
 */
@Stateless
@Path("region")
public class WebServRestRegion {
    
    @Inject ModeleWebServRegion modele;
    
    @GET
    @Path("tous/resumes")
    @Produces({"application/xml","application/json"})
    public List<ResumeRegion> getTousLesResumeRegions() {
        
        return modele.getTousLesResumeRegions();
    }

}
