/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package webservice.rest.commande;

import dao.commande.DaoCommande;
import dto.commande.ResumeCommande;
import dto.lignedecommande.ResumeLigneDeCommande;
import entites.Commande;
import entites.LigneDeCommande;
import fabrique.dto.commande.FabResumesCommandes;
import fabrique.dto.lignedecommande.FabResumeLigneDeCommande;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

/*
* @author Jean-Baptiste Ferrere
*/
@Stateless
@Path("commande")
public class WebServRestCommande  {

    @Inject  ResumeCommande             resumecom;
    @Inject  FabResumesCommandes        fabcom;
    @Inject  DaoCommande                dao;
    @Inject  FabResumeLigneDeCommande   fabldc;
    
    @GET
    @Path("{numeroCommande}")
    @Produces({"application/xml","application/json"})
    public ResumeCommande commandeparnumcommande(@PathParam("numeroCommande")Long numCom) {
       
       ResumeCommande rc = fabcom.creerResumeCommande(numCom);
       return rc;
    }
    
    @GET
    @Path("toutes")
    @Produces({"application/xml","application/json"})
    public List<ResumeCommande> touteslescommandes(){
 
        List<Commande> lesCommandes = dao.getToutesLesCommandes();
        
        for(Commande c : lesCommandes){
            
           fabcom.creerResumeCommande(c);
        }
        
        List<ResumeCommande> lesResumesCommandes=fabcom.getResumesCommande(lesCommandes);
       return lesResumesCommandes;
    }
    
    @GET
    @Path("lignesdecommandes/{numeroCommande}")
    @Produces({"application/xml","application/json"})
    public List<ResumeLigneDeCommande> LigneCommande(@PathParam("numeroCommande")Long numCom){
 
        Commande cmd =  dao.getCommande(numCom);
        List<LigneDeCommande> lignesDeCommandes= cmd.getLesLignesDeCommande();
        List<ResumeLigneDeCommande> resumesLigneDeCommande = fabldc.creerListeResumeLigneCommande(lignesDeCommandes);
  
        return resumesLigneDeCommande;
    }
    
}