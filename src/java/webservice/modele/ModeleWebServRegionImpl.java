
package webservice.modele;

import bal.region.BalRegion;
import dao.region.DaoRegion;
import dto.region.ResumeRegion;
import entites.Region;
import fabrique.dto.region.FabResumeRegion;
import java.io.Serializable;
import java.util.List;
import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class ModeleWebServRegionImpl implements Serializable, ModeleWebServRegion{
    
    @Inject DaoRegion           dao;
    @Inject BalRegion           bal;
    @Inject FabResumeRegion     fab;
    
    ///////////////////////////////////////////////////////////////////////////////////////////////
    
    @Override
    public Region getLaRegion (String pcodeReg){
        return dao.getLaRegion(pcodeReg);
    }
    
    @Override
    public List<Region> getTousLesRegions() {
        return dao.getToutesLesRegions();
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////
    
    @Override
    public Float getCA (Region pRegion){
    
        return bal.caAnneeEnCours(pRegion);
    }
    
    @Override
    public Float getNbClient (Region pRegion){
    
        return bal.nbClient(pRegion);
    }
    
    /////////////////////////////////////////////////////////////////////////////////////////////// 
    
    @Override
    public ResumeRegion getLeResumeRegion (String pcodeReg){
    
        return fab.getResumeRegion(pcodeReg);
    }
    
    @Override
    public List<ResumeRegion> getTousLesResumeRegions() {
        return fab.getTousLesResumeRegions();
    }
    
}
