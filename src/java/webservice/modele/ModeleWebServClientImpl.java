package webservice.modele;

import bal.client.BalClient;
import dao.client.DaoClient;
import fabrique.dto.client.FabResumeClient;
import dto.client.ResumeClient;
import fabrique.dto.commande.FabResumesCommandes;
import dto.commande.ResumeCommande;
import entites.Client;
import java.io.Serializable;
import java.util.List;
import javax.inject.Inject;
import javax.inject.Singleton;
import fabrique.arbres.client.FabArbresClient;

@Singleton

public class ModeleWebServClientImpl implements Serializable, ModeleWebServClient{
    
    @Inject DaoClient            daoClient;
    @Inject BalClient            balClient;
    @Inject FabArbresClient      arbresClient;
    @Inject FabResumeClient      fabResumeClients;

    @Inject FabResumesCommandes  fabResumeCommandes;
   
    ///////////////////////////////////////////////////////////////////////////////////////////////
    
    @Override
    public Client getLeClient(Long pNumcli) {
        return daoClient.getLeClient(pNumcli);
    }
    
    @Override
    public List<Client> getTousLesClients() {
        return daoClient.getTousLesClients();
    }
    
    ///////////////////////////////////////////////////////////////////////////////////////////////
    
    @Override
    public Float caAnneeEnCours(Client pClient) {
        return balClient.caAnneeEnCours(pClient);
    }

    @Override
    public Float resteARegler(Client pClient) {
        return balClient.resteARegler(pClient);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    
    @Override
    public void creerArbreClientV1(Client client) {
        arbresClient.creerArbreClientV1(client);
    }

    @Override
    public void creerArbreClientV2(Client client) {
        arbresClient.creerArbreClientV2(client);
    }

    //////////////////////////////////////////////////////////////////////////////////////////////
    
    @Override
    public ResumeClient getLeResumeClient(Long pNumcli) {
        return fabResumeClients.getResumeClient(pNumcli);
    }   

    @Override
    public List<ResumeClient> getTousLesResumeClients() {
        return fabResumeClients.getTousLesResumeClients();
    }
    
    
    ///////////////////////////////////////////////////////////////////////////////////////////////
    
    @Override
    public List<ResumeCommande> getResumesCommandeClient(Client client) {
         
        return  fabResumeCommandes.getResumesCommande(client.getLesCommandes());
    }

    @Override
    public List<ResumeCommande> getResumesCommandeClient(Long numcli){
       
        return getResumesCommandeClient(numcli);
    }
}
