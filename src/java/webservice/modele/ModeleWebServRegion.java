
package webservice.modele;

import dto.region.ResumeRegion;
import entites.Region;
import java.util.List;


public interface ModeleWebServRegion {
    
    Region              getLaRegion             (String pcodeReg);
    
    List<Region>        getTousLesRegions       ();
    
    Float               getCA                   (Region pRegion);
    
    Float               getNbClient             (Region pRegion);
    
    ResumeRegion        getLeResumeRegion       (String pcodeReg);
    
    List<ResumeRegion>  getTousLesResumeRegions (); 
    
    
    
}
