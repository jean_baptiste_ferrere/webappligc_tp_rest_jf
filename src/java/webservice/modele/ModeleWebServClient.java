
package webservice.modele;

import dto.client.ResumeClient;
import dto.commande.ResumeCommande;
import entites.Client;
import java.util.List;

public interface ModeleWebServClient {

    Float caAnneeEnCours(Client pClient);

    List<ResumeCommande> getResumesCommandeClient(Client client);
     
    List<ResumeCommande> getResumesCommandeClient(Long pNumcli);
    
    List<Client> getTousLesClients();

    Float resteARegler(Client pClient);

    Client getLeClient(Long pNumcli);

    void creerArbreClientV1(Client client);

    void creerArbreClientV2(Client client);

    ResumeClient getLeResumeClient(Long pNumcli);

    List<ResumeClient> getTousLesResumeClients();
    
}
