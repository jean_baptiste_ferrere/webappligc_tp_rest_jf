//
package webservice.soap.client;

import dto.client.ResumeClient;
import dto.commande.ResumeCommande;
import entites.Client;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import webservice.modele.ModeleWebServClient;
@Stateless
@WebService(serviceName = "WebServClient")
public class WebServClient {
     
    @Inject ModeleWebServClient modele;
    
    
    @WebMethod(operationName = "getClient")
    public Client getClient(@WebParam(name = "numcli") Long numcli) {
        
       Client client=modele.getLeClient(numcli); 
       modele.creerArbreClientV2(client);
       return client;
    }

    @WebMethod(operationName = "getTouslesClients")
    public List<Client> getTouslesClients() {
       
       List<Client> lc=  modele.getTousLesClients();
       for (Client c : lc){modele.creerArbreClientV1(c);}
       return lc;
    }

    @WebMethod(operationName = "getResumeClient")
    public ResumeClient getResumeClient(@WebParam(name = "pNumcli") Long pNumcli) {
       
        return modele.getLeResumeClient(pNumcli);
    }
    
    @WebMethod(operationName = "getLesResumesCommandeClient")
    public List<ResumeCommande> getLesResumesCommandeClient(@WebParam(name = "numcli") Long numcli){
        
        Client client=modele.getLeClient(numcli);
        return modele.getResumesCommandeClient(client);
    }


    @WebMethod(operationName = "caAnneeEncoursClient")
    public Float caAnneeEncoursClient(@WebParam(name = "numcli") Long numcli) {
        
        Client client=modele.getLeClient(numcli);
        return modele.caAnneeEnCours(client);   
    }

    @WebMethod(operationName = "getSoldeClient")
    public Float getSoldeClient(@WebParam(name = "pNumcli") Long pNumcli) {
        
        Client cli=modele.getLeClient(pNumcli);
        return -modele.resteARegler(cli);
    }

    
    @WebMethod(operationName = "getTousLesResumeCLients")
    public List<ResumeClient> getTousLesResumeCLients() {
        
        return modele.getTousLesResumeClients();
    }
  
}
