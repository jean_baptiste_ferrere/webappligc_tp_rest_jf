
package webservice.soap.initbase;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jws.WebService;
import javax.jws.WebMethod;
import jeudessais.JeuDEssai;

/**
 *
 * @author rsmon
 */
@Stateless
@WebService(serviceName = "ServiceInitBase")
public class ServiceSoapInitBase {

    @Inject JeuDEssai jeudEssai;

    @WebMethod(operationName = "reinitialiserBase")
    public String reinitialiserBase() {
        
        return jeudEssai.reinitialiserBDD();
        
    }
       
}
