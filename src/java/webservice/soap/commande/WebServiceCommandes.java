
package webservice.soap.commande;

import dao.commande.DaoCommande;
import fabrique.dto.commande.FabResumesCommandesImpl;
import dto.commande.ResumeCommande;
import fabrique.dto.lignedecommande.FabResumeLigneDeCommande;
import dto.lignedecommande.ResumeLigneDeCommande;
import entites.Commande;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author rsmon
 */
@Stateless
@WebService(serviceName = "WebServiceCommandes")
public class WebServiceCommandes {

    @Inject DaoCommande                  daoCommande; 
    @Inject FabResumesCommandesImpl      fabResuCom;
    @Inject FabResumeLigneDeCommande     fabResuLigneCom;
    
    @WebMethod(operationName = "getResumeCommande")
    public ResumeCommande getResumeCommande(@WebParam(name = "numcom") Long numcom) {
        return fabResuCom.creerResumeCommande(numcom);
    }

    @WebMethod(operationName = "getLesResumesLigneDeCommandes")
    public List<ResumeLigneDeCommande> getLesResumesLigneDeCommandes(@WebParam(name = "pNumcom") Long pNumcom) {
       
        Commande com= daoCommande.getCommande(pNumcom);
        
        return fabResuLigneCom.creerListeResumeLigneCommande(com.getLesLignesDeCommande());
    }
      
}
