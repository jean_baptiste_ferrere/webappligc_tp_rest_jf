
package dto.lignedecommande;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author rsmon
 */

@XmlRootElement
public class ResumeLigneDeCommande {
  
  private String refProd;
  private String desigProd;
  private Float  prixProd;
  private Float  qtecom;
  private Float  montantLigneHT;
  private Float  montantLigneTTC;
  
  //<editor-fold defaultstate="collapsed" desc="getters et setters">
  public String getRefProd() {
      return refProd;
  }
  
  public void setRefProd(String refProd) {
      this.refProd = refProd;
  }
  
  public String getDesigProd() {
      return desigProd;
  }
  
  public void setDesigProd(String desigProd) {
      this.desigProd = desigProd;
  }
  
  public Float getPrixProd() {
      return prixProd;
  }
  
  public void setPrixProd(Float prixProd) {
      this.prixProd = prixProd;
  }

    public Float getQtecom() {
        return qtecom;
    }

    public void setQtecom(Float qtecom) {
        this.qtecom = qtecom;
    }
  
  
  public Float getMontantLigneHT() {
      return montantLigneHT;
  }
  
  public void setMontantLigneHT(Float montantLigneHT) {
      this.montantLigneHT = montantLigneHT;
  }
  
  public Float getMontantLigneTTC() {
      return montantLigneTTC;
  }
  
  public void setMontantLigneTTC(Float montantLigneTTC) {
      this.montantLigneTTC = montantLigneTTC;
  }
  
  
  
  //</editor-fold>
    
}
