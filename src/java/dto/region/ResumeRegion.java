
package dto.region;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ResumeRegion {
    
    private String codeReg;
    private String nomReg;
    
    private Float ca;
    private Float nbClient;

    //<editor-fold defaultstate="collapsed" desc="getters et setters">
    
    public String getCodeReg() {
        return codeReg;
    }

    public void setCodeReg(String codeReg) {
        this.codeReg = codeReg;
    }

    public String getNomReg() {
        return nomReg;
    }

    public void setNomReg(String nomReg) {
        this.nomReg = nomReg;
    }

    public Float getCa() {
        return ca;
    }

    public void setCa(Float ca) {
        this.ca = ca;
    }

    public Float getNbClient() {
        return nbClient;
    }

    public void setNbClient(Float nbClient) {
        this.nbClient = nbClient;
    }
    
    //</editor-fold>
    
}
