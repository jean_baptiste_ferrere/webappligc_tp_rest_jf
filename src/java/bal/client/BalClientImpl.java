package bal.client;
import bal.commande.BalCommande;
import dao.commande.DaoCommande;
import entites.Client;
import entites.Commande;
import java.io.Serializable;
import javax.inject.Inject;
import javax.inject.Singleton;
import static utilitaires.UtilDate.*;

@Singleton 

public class BalClientImpl  implements BalClient, Serializable{
    
    @Inject BalCommande  balCommande;
    @Inject DaoCommande  daoCommande;
    
    @Override
    public Float                 caAnnuel(Client pClient, int pAnnee) {
        
        Float ca=0f;
        for (Commande cmd : daoCommande.getLesCommandesduClient(pClient, pAnnee)){
               
               if(cmd.getEtatCom().equalsIgnoreCase("R") && 
                   dansAnnee(cmd.getDateCom(), pAnnee)
                 )
               {
                   ca+=balCommande.montantCommandeHT(cmd);
               } 
        }
        return ca;
    }

    @Override
    public Float                 caMensuel(Client pClient, int pAnnee, int pMois) {
       
        Float ca=0f;
        for (Commande cmd : daoCommande.getLesCommandesduClient(pClient, pAnnee, pMois)){
               
               if( cmd.getEtatCom().equalsIgnoreCase("R")&&
                   dansAnneeEtMois(cmd.getDateCom(), pAnnee, pMois)
                 )
               {
                       
                  ca+=balCommande.montantCommandeHT(cmd);
               }
        }
        return ca;
    } 
    
    @Override
    public Float caAnneeEnCours(Client pClient) {
        return this.caAnnuel(pClient, anneeCourante());
    }

    @Override
    public Float caMoisEnCours(Client pClient) {
        return this.caMensuel(pClient, anneeCourante(), moisCourant());
    } 
    
   
    @Override
    public Float resteARegler(Client pClient) {
       
       Float valeur=0F;
       
       for ( Commande cmd : pClient.getLesCommandes()) {
       
         if (! balCommande.estReglee(cmd) ){
             valeur+= balCommande.montantCommandeTTC(cmd);
         }
       }
       return valeur;
       
    }

    @Override
    public Float soldeClient(Client pClient){
       return -resteARegler(pClient);
    }
               
}
