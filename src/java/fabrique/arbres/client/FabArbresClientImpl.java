
package fabrique.arbres.client;

import dao.Dao;
import entites.CategorieProduit;
import entites.Client;
import entites.Commande;
import entites.LigneDeCommande;
import entites.Produit;
import entites.Region;
import java.io.Serializable;
import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class FabArbresClientImpl implements Serializable, FabArbresClient{
  
    @Inject Dao dao;
    
    @Override
    public void creerArbreClientV1(Client client){
            
       dao.detache(client);
       Region region=client.getLaRegion();
       dao.detache(region);
       region.setLesClients(null);
       client.setLesCommandes(null);
    }

    @Override
    public void creerArbreClientV2(Client client){
    
       dao.detache(client);
       Region region=client.getLaRegion();
       dao.detache(region);
       region.setLesClients(null);
      
       for (Commande cmd : client.getLesCommandes()){
      
          dao.detache(cmd);
          cmd.setLeClient(null);
          
          for (LigneDeCommande lgc : cmd.getLesLignesDeCommande()){
             dao.detache(lgc);
             lgc.setLaCommande(null);
             Produit p=lgc.getLeProduit();
             dao.detache(p);
             p.setLesLignesDeCommande(null);
             CategorieProduit categorie=p.getLaCategorie();
             dao.detache(categorie);
             categorie.setLesProduits(null);  
          }     
        }
     }

}
