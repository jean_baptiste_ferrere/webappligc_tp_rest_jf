
package fabrique.dto.region;

import dto.region.ResumeRegion;
import entites.Region;
import java.util.List;


public interface FabResumeRegion {
    
    ResumeRegion        getResumeRegion (Region pRegion);
    
    ResumeRegion        getResumeRegion (String pcodeReg);
    
    List<ResumeRegion>  getTousLesResumeRegions();
}
