
package fabrique.dto.region;

import bal.region.BalRegion;
import dao.region.DaoRegion;
import dto.region.ResumeRegion;
import entites.Region;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class FabResumeRegionImpl implements Serializable, FabResumeRegion {
    
    @Inject DaoRegion dao;
    @Inject BalRegion bal;
    
    @Override
    public ResumeRegion getResumeRegion (Region pRegion){

    ResumeRegion rr=new ResumeRegion();    
    rr.setCodeReg(pRegion.getCodeRegion());
    rr.setNomReg(pRegion.getNomRegion());
    
    rr.setCa(bal.caAnneeEnCours(pRegion));
    rr.setNbClient(bal.nbClient(pRegion));
          
    return rr;
    
    }
    
    
    @Override
    public ResumeRegion getResumeRegion (String pcodeReg){
        return getResumeRegion(dao.getLaRegion(pcodeReg));
    }
    
    @Override
    public List<ResumeRegion> getTousLesResumeRegions(){
        return getLesResumeRegions(dao.getToutesLesRegions());
    }
    
    private List<ResumeRegion> getLesResumeRegions(List<Region> pListeRegions){
        
        List<ResumeRegion> lr = new LinkedList<ResumeRegion>();
        
        for(Region r : pListeRegions){
           lr.add(getResumeRegion(r));
        }
        return lr;
    }
}
