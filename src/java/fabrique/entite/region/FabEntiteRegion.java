
package fabrique.entite.region;

import entites.Region;
import javax.inject.Singleton;

/**
 *
 * @author rsmon
 */

@Singleton
public class FabEntiteRegion {
   
    public Region creerEntiteRegion(String codeRegion, String nomRegion) {
        
        Region region= new Region();
        
        region.setCodeRegion(codeRegion);
        region.setNomRegion(nomRegion);
        
        return region;
    }    
}
